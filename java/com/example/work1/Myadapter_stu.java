package com.example.work1;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;


public class Myadapter_stu extends RecyclerView.Adapter<Myadapter_stu.MyHolder>{

    List<Student> stulist;

    public Myadapter_stu(List<Student> stu) {
        stulist=stu;
    }

    @Override
    public MyHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view=LayoutInflater.from(parent.getContext()).inflate(R.layout.item,parent,false);

        MyHolder myHolder=new MyHolder(view);

        return myHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyHolder holder, int position) {
        Student student=stulist.get(position);
        holder.nametext.setText(student.getName());
        holder.idtext.setText(student.getId());
    }

    @Override
    public int getItemCount() {
        return (stulist.size());
    }

    //行中有哪些控件由holder决定
    class MyHolder extends RecyclerView.ViewHolder{
        TextView nametext;
        TextView idtext;

        public MyHolder(@NonNull View itemView) {
            super(itemView);
            nametext=itemView.findViewById(R.id.textView21);
            idtext=itemView.findViewById(R.id.textView22);
        }
    }

}
