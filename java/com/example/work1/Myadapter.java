package com.example.work1;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;


public class Myadapter extends RecyclerView.Adapter<Myadapter.MyHolder>{

    List<String> list1;
    Context context1;

    OnItemClickListener listener;
    public interface OnItemClickListener {
        void onItemClick(String content);
    }
    public Myadapter(List list, Context context,OnItemClickListener listener1) {
        this.list1 = list;
        this.context1 = context;
        listener = listener1;
    }

    @Override
    public MyHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view=LayoutInflater.from(context1).inflate(R.layout.item,parent,false);

        MyHolder myHolder=new MyHolder(view);

        return myHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyHolder holder, int position) {
        String item = list1.get(position);
        holder.textView.setText(list1.get(position));
        // 为项目设置点击监听器
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onItemClick(item);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return (list1.size());
    }

    //行中有哪些控件由holder决定
    class MyHolder extends RecyclerView.ViewHolder{
        TextView textView;

        public MyHolder(@NonNull View itemView) {
            super(itemView);
            textView=itemView.findViewById(R.id.textView21);
//
        }
    }

}

