package com.example.work1;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import androidx.fragment.app.Fragment;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

public class MainActivity extends AppCompatActivity implements  View.OnClickListener{
    Fragment fragment1;
    Fragment fragment2;
    Fragment fragment3;
    Fragment fragment4;
    Fragment fragmentlist1;
    Fragment fragmentlist2;
    FragmentManager fm;


    LinearLayout linearLayout1,linearLayout2,linearLayout3,linearLayout4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        linearLayout1 = findViewById(R.id.linearlayout1);
        linearLayout2 = findViewById(R.id.linearlayout2);
        linearLayout3 = findViewById(R.id.linearlayout3);
        linearLayout4 = findViewById(R.id.linearlayout4);

        fm=getSupportFragmentManager();
        fragment1 = new Fragment1();
        fragment2 = new Fragment2();
        fragment3 = new Fragment3();
        fragment4 = new Fragment4();
        fragmentlist1 = new Fragment_list1();
        fragmentlist2 = new Fragment_list2();
        innitial();
        fragmentshow(fragmentlist2);

        linearLayout1.setOnClickListener(this);
        linearLayout2.setOnClickListener(this);
        linearLayout3.setOnClickListener(this);
        linearLayout4.setOnClickListener(this);

    }
    private void fragmentHide() {
        FragmentTransaction ft =fm.beginTransaction()
                .hide(fragmentlist2)
                .hide(fragmentlist1)
                .hide(fragment3)
                .hide(fragment4);
        ft.commit();
    }
    private void fragmentshow(Fragment fragment) {
        fragmentHide();
        FragmentTransaction ft =fm.beginTransaction()
                .show(fragment);
        ft.commit();
    }

    private void innitial() {
        FragmentTransaction ft=fm.beginTransaction()
                .add(R.id.content,fragmentlist2)
                .add(R.id.content,fragmentlist1)
                .add(R.id.content,fragment3)
                .add(R.id.content,fragment4);
        ft.commit();
    }

    @Override
    public void onClick(View view) {
        if(view.getId()==R.id.linearlayout1)
                fragmentshow(fragmentlist2);
        else if(view.getId()==R.id.linearlayout2)
            fragmentshow(fragmentlist1);
        else if(view.getId()==R.id.linearlayout3)
            fragmentshow(fragment3);
        else if(view.getId()==R.id.linearlayout4)
            fragmentshow(fragment4);

    }
}