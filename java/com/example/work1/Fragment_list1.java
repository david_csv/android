package com.example.work1;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link Fragment_list1#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Fragment_list1 extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public Fragment_list1() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Fragment_list1.
     */
    // TODO: Rename and change types and number of parameters
    public static Fragment_list1 newInstance(String param1, String param2) {
        Fragment_list1 fragment = new Fragment_list1();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    private RecyclerView recyclerView;
    private Myadapter myAdapter;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_list1, container, false);
        recyclerView = view.findViewById(R.id.recycleView_list1);
//        List<String> list = new ArrayList<>();
//        for (int i = 0; i < 9; i++) {
//            list.add("这是第" + i + "个例子");
//        }
        List<Student> stuL=new ArrayList<>();
        Student s1=new Student("戴维","201");
        Student s2=new Student("张三","202");
        Student s3=new Student("张四","203");
        Student s4=new Student("张五","204");
        Student s5=new Student("张六","205");
        Student s6=new Student("张七","206");
        stuL.add(s1);
        stuL.add(s2);
        stuL.add(s3);
        stuL.add(s4);
        stuL.add(s5);
        stuL.add(s6);
        Myadapter_stu myAdapter= new Myadapter_stu(stuL);
//        myAdapter = new Myadapter(list, requireContext()); // Use requireContext() to get the Fragment's context
        recyclerView.setAdapter(myAdapter);

        LinearLayoutManager manager = new LinearLayoutManager(requireContext());
        manager.setOrientation(RecyclerView.VERTICAL);
        recyclerView.setLayoutManager(manager);

        return view;



    }
}